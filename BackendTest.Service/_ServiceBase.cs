﻿using BackendTest.Core.Data;
using BackendTest.Core.Models;
using BackendTest.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BackendTest.Service
{
    public class _ServiceBase<TModel> : _IService<TModel> where TModel : _ModelBase
    {
        public _ServiceBase(ISchoolDataContext schoolDataContext)
        {
            SchoolDataContext = schoolDataContext;
            Items = schoolDataContext.GetQueryable<TModel>();
        }
        public ISchoolDataContext SchoolDataContext { get; }

        public IQueryable<TModel> Items { get; }
    }
}
