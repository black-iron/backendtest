﻿using BackendTest.Core.Data;
using BackendTest.Core.Models;
using BackendTest.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BackendTest.Service
{
    public class StudentService : _ServiceBase<Student>, IStudentService
    {
        public StudentService(ISchoolDataContext schoolDataContext) : base(schoolDataContext)
        {
        }

        public void Add(Student student)
        {
            var classModel = SchoolDataContext.Classes.Find(student.Class.Id);
            student.Class = classModel;
            SchoolDataContext.Students.Add(student);
            SchoolDataContext.Save();
        }
    }
}
