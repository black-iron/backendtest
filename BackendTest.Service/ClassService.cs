﻿using BackendTest.Core.Data;
using BackendTest.Core.Models;
using BackendTest.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BackendTest.Service
{
    public class ClassService : _ServiceBase<Class>, IClassService
    {
        public ClassService(ISchoolDataContext schoolDataContext) : base(schoolDataContext)
        {
        }

        public void Add(Class classModel)
        {
            SchoolDataContext.Classes.Add(classModel);
            SchoolDataContext.Save();
        }

        public void Delete(int id)
        {
            SchoolDataContext.Classes.Remove(new Class { Id = id });
            SchoolDataContext.Save();
        }

        public Class Find(int id)
        {
            return Items.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Class> GetAll()
        {
            return Items.ToList();
        }

        public void Update(Class classModel)
        {
            var lastClass = Items.FirstOrDefault(x => x.Id == classModel.Id);
            lastClass.ClassName = classModel.ClassName;
            lastClass.Location = classModel.Location;
            lastClass.TeacherName = classModel.TeacherName;
            SchoolDataContext.Save();
        }
    }
}
