﻿using BackendTest.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BackendTest.Core.Service
{
    public interface IStudentService : _IService<Student>
    {
        void Add(Student student);
    }
}
