﻿using BackendTest.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BackendTest.Core.Service
{
    public interface IClassService : _IService<Class>
    {
        IEnumerable<Class> GetAll();
        void Add(Class classModel);
        Class Find(int id);
        void Update(Class classModel);
        void Delete(int id);
    }
}
