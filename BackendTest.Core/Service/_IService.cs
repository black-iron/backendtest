﻿using BackendTest.Core.Data;
using BackendTest.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BackendTest.Core.Service
{
    public interface _IService<TModel> where TModel : _ModelBase
    {
        ISchoolDataContext SchoolDataContext { get; }
        IQueryable<TModel> Items { get; }
    }
}
