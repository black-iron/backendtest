﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendTest.Core.Models
{
    public class Class:_ModelBase
    {
        public string ClassName { get; set; }
        public string Location { get; set; }
        public string TeacherName { get; set; }
    }
}
