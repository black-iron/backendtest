﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendTest.Core.Models
{
    public class Student : _ModelBase
    {
        public string StudentName { get; set; }
        public byte Age { get; set; }
        public float Gpa { get; set; }
        public Class Class { get; set; }
    }
}
