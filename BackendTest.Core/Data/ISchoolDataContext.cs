﻿using BackendTest.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BackendTest.Core.Data
{
    public interface ISchoolDataContext
    {
        void Save();
        IQueryable<TModel> GetQueryable<TModel>() where TModel : _ModelBase;
        DbSet<Class> Classes { get; set; }
        DbSet<Student> Students { get; set; }
    }
}
