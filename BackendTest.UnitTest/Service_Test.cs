using BackendTest.Core.Models;
using BackendTest.Core.Service;
using BackendTest.Data;
using BackendTest.Service;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using Xunit;

namespace BackendTest.UnitTest
{
    public class Service_Test
    {
        private IClassService _classService
        {
            get
            {


                var dcos = new DbContextOptionsBuilder<SchoolDataContext>();
                dcos.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=SchoolDataContext;Trusted_Connection=True;ConnectRetryCount=0");
                var dbContext = new SchoolDataContext(dcos.Options);
                return new ClassService(dbContext);
            }
        }
        [Fact]
        public void Test_1_ClassService_Add()
        {
            var classModel = new Class
            {
                ClassName = "e6031bb7-3da8-4340-a627-2db4f1d29801",
                Location = "TEST",
                TeacherName = "TEST"
            };
            _classService.Add(classModel);
            var itemAdded = _classService.Find(classModel.Id);
            Assert.NotNull(itemAdded);
        }
        [Fact]
        public void Test_2_ClassService_Read()
        {
            var item = _classService.Items.First(x => x.ClassName == "e6031bb7-3da8-4340-a627-2db4f1d29801");
            Assert.NotNull(item);
        }

        [Fact]
        public void Test_3_ClassService_Update()
        {

            var classModel = _classService.Items.FirstOrDefault(x => x.ClassName == "e6031bb7-3da8-4340-a627-2db4f1d29801");
            var oldModel = new Class
            {
                ClassName = classModel.ClassName,
                Location = classModel.Location,
                TeacherName = classModel.TeacherName
            };
            classModel.ClassName = "e6031bb7-3da8-4340-a627-2db4f1d29801";
            classModel.Location = "TEST1";
            classModel.TeacherName = "TEST1";
            _classService.Update(classModel);
            var itemAdded = _classService.Find(classModel.Id);
            Assert.NotEqual(oldModel.Location, classModel.Location);
            Assert.NotEqual(oldModel.TeacherName, classModel.TeacherName);
        }
        [Fact]
        public void Test_4_ClassService_Delete()
        {
            var classModel = _classService.Items.FirstOrDefault(x => x.ClassName == "e6031bb7-3da8-4340-a627-2db4f1d29801");
            _classService.Delete(classModel.Id);
            var itemAdded = _classService.Find(classModel.Id);
            Assert.Null(itemAdded);
        }

    }
}
