﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BackendTest.UserInterface.Models;
using BackendTest.Core.Models;
using Microsoft.EntityFrameworkCore;
using BackendTest.Core.Service;

namespace BackendTest.UserInterface.Controllers
{
    public class StudentsController : Controller
    {
        private readonly IClassService _classService;
        private readonly IStudentService _studentService;
        public StudentsController(IClassService classService, IStudentService studentService)
        {
            _classService = classService;
            _studentService = studentService;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Create(int classId)
        {
            return View(new Student()
            {
                Id = 0,
                Class = new Class
                {
                    Id = classId
                }
            });
        }
        [HttpPost]
        public async Task<IActionResult> Create(Student student)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    student.Id = 0;
                    _studentService.Add(student);
                    return RedirectToAction("Index","Classes");
                }
            }
            catch (DbUpdateException ex )
            {
                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
            }
            return View(student);
        }
    }
}
