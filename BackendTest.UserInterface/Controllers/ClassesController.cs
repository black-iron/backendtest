﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BackendTest.UserInterface.Models;
using BackendTest.Core.Service;
using Microsoft.EntityFrameworkCore;
using BackendTest.Core.Models;

namespace BackendTest.UserInterface.Controllers
{
    public class ClassesController : Controller
    {
        private readonly IClassService _classService;
        private readonly IStudentService _studentService;
        public ClassesController(IClassService classService, IStudentService studentService)
        {
            _classService = classService;
            _studentService = studentService;
        }
        public async Task<IActionResult> Index(string sortOrder)
        {
            ViewData["ClassNameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "classname_desc" : "";
            ViewData["LocationSortParm"] = sortOrder == "Location" ? "date_desc" : "Location";
            ViewData["TeacherNameSortParm"] = sortOrder == "TeacherName" ? "date_desc" : "TeacherName";
            var classes = _classService.Items;
            switch (sortOrder)
            {
                case "classname_desc":
                    classes = classes.OrderByDescending(s => s.ClassName);
                    break;
                case "Location":
                    classes = classes.OrderBy(s => s.Location);
                    break;
                case "date_desc":
                    classes = classes.OrderByDescending(s => s.Location);
                    break;
                case "TeacherName":
                    classes = classes.OrderBy(s => s.TeacherName);
                    break;
                case "teachername_desc":
                    classes = classes.OrderByDescending(s => s.TeacherName);
                    break;
                default:
                    classes = classes.OrderBy(s => s.ClassName);
                    break;
            }
            ViewData["Students"] = await _studentService.Items.Include(x=>x.Class).AsNoTracking().ToListAsync();
            return View(await classes.AsNoTracking().ToListAsync());
        }
        public IActionResult Create()
        {
            return View(new Class());
        }
        [HttpPost]
        public async Task<IActionResult> Create([Bind("ClassName,Location,TeacherName")] Class classModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _classService.Add(classModel);
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateException /* ex */)
            {
                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
            }
            return View(classModel);
        }
        public IActionResult Edit(int id)
        {
            var classModel = _classService.Find(id);
            return View(classModel);
        }
        [HttpPost]
        public async Task<IActionResult> Edit([Bind("Id,ClassName,Location,TeacherName")] Class classModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _classService.Update(classModel);
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateException /* ex */)
            {
                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
            }
            return View(classModel);
        }
        public IActionResult Delete(int id)
        {
            var classModel = _classService.Find(id);
            return View(classModel);
        }
        [HttpPost]
        public IActionResult Delete(int id,bool confirm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _classService.Delete(id);
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateException /* ex */)
            {
                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
            }
            return View(id);
        }
    }
}
