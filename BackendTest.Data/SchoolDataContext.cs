﻿using BackendTest.Core.Data;
using BackendTest.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BackendTest.Data
{
    public class SchoolDataContext : DbContext, ISchoolDataContext
    {
        public SchoolDataContext(DbContextOptions<SchoolDataContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public IQueryable<TModel> GetQueryable<TModel>() where TModel : _ModelBase
        {
            return Set<TModel>();
        }

        public void Save()
        {
            SaveChanges();
        }

        public DbSet<Class> Classes { get; set; }
        public DbSet<Student> Students { get; set; }
    }
}
